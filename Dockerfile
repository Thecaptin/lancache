FROM nginx:mainline-alpine

RUN apk add \
  --no-cache \
  --repository http://dl-cdn.alpinelinux.org/alpine/edge/community \
  --update \
  shadow \
  su-exec \
  bash \
  supervisor \
  nano

VOLUME ["/var/lancache", "/var/www", "/etc/nginx/lansites"]

RUN ln -sf /dev/stdout /var/log/nginx/access.log ; \
    ln -sf /dev/stderr /var/log/nginx/error.log

ADD lancache/nginx.conf /etc/nginx/nginx.conf
ADD lancache/lancache/ /etc/nginx/lancache
ADD lancache/sites/ /etc/nginx/sites/
ADD conf/supervisord.conf /etc/supervisord.conf

COPY entrypoint.sh /entrypoint.sh

RUN sed -i 's|user www-data www-data;|user nginx nginx;|g' /etc/nginx/nginx.conf ; \
    chmod +x /entrypoint.sh ; \
    mkdir -p /data/www/cache/steam/serverlist ; \
    chown -R nginx. /var/lancache /var/www /data ; \
    mkdir -p /etc/default/ ; \
    touch /tmp/supervisor.sock ; \
    chmod 777 /tmp/supervisor.sock

EXPOSE 80 443

ENTRYPOINT ["/entrypoint.sh"]
