# Pull latest changes
```
git pull
```

# Stop existing container
```
docker stop lancache
```

# Remove old container
```
docker rm lancache
```

# Build new container
```
docker build -t slan/lancache .
```

# Configure new container
```
docker run --restart=always -d  -v /slan/cache:/var/lancache -v /slan/sites/www:/var/www -v /slan/sites/nginx:/etc/nginx/lansites --name lancache -p 53:53/udp -p 80:80 -p 443:443 --cap-add=NET_ADMIN slan/lancache:latest
```

# Login into container shell
```
docker exec -it lancache /bin/sh
```

# List all containers
```
docker ps -a
```

# Restart nginx confs on-the-fly
```
docker exec -it lancache supervisorctl restart nginx
```

# Check the processes statuses
```
docker exec -it lancache supervisorctl status
```

# Watch logs
```
docker logs -f lancache
```

# "Oneliner" (2017-10-29 15:50:00)
```
cd /slan/docker/lancache &&
git pull &&
docker stop lancache ;
docker rm lancache ;
docker build -t slan/lancache . &&
docker run --restart=always -d  -v /slan/cache:/var/lancache -v /slan/sites/www:/var/www -v /slan/sites/nginx:/etc/nginx/lansites --name lancache -p 80:80 -p 443:443 --cap-add=NET_ADMIN slan/lancache:latest &&
docker exec -it lancache /bin/sh
```
